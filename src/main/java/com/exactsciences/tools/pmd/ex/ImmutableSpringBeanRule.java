package com.exactsciences.tools.pmd.ex;

import net.sourceforge.pmd.lang.java.ast.*;
import net.sourceforge.pmd.lang.java.rule.AbstractJavaRule;
import net.sourceforge.pmd.properties.PropertyDescriptor;
import net.sourceforge.pmd.properties.PropertyFactory;

import java.util.Set;

public class ImmutableSpringBeanRule extends AbstractJavaRule {

    /**
     * Property descriptor.
     */
    private static final PropertyDescriptor<Boolean> IGNORE_TESTS =
            PropertyFactory.booleanProperty("ignoreTests")
                    .desc("Ignore test classes")
                    .defaultValue(true)
                    .build();

    private static final Set<String> SPRING_BEAN_ANNOTATIONS = Set.of("Service", "Component");
    private static final Set<String> SKIP_FIELDS_WITH_ANNOTATIONS = Set.of(
            "net.devh.boot.grpc.client.inject.GrpcClient",
            "org.springframework.beans.factory.annotation.Value"
    );

    public ImmutableSpringBeanRule() {
        definePropertyDescriptor(IGNORE_TESTS);
    }

    @Override
    public Object visit(ASTFieldDeclaration node, Object data) {
        var ignoreTests = getProperty(IGNORE_TESTS);
        if (isClass(node) && (!ignoreTests || !isTest(node)) && isntFinalOrValue(node) && isSpringBean(node)) {
            asCtx(data).addViolationWithMessage(
                    node,
                    "Field '" + node.getVariableName() + "' should be final in Spring Bean"
            );
        }
        return data;
    }

    private boolean isClass(ASTFieldDeclaration node) {
        try {
            var clazz = getAstClassOrInterfaceDeclaration(node);
            return clazz != null;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean isTest(ASTFieldDeclaration node) {
        var clazz = getAstClassOrInterfaceDeclaration(node);
        var path = clazz.getRoot().getAstInfo().getTextDocument().getFileId().getAbsolutePath();
        //System.out.println("------------Path: " + path);
        return path.contains("\\src\\test\\");
    }

    private static boolean isntFinalOrValue(ASTFieldDeclaration node) {
        return !node.hasModifiers(JModifier.FINAL)
                && node.getDeclaredAnnotations().none(annotation -> SKIP_FIELDS_WITH_ANNOTATIONS.contains(annotation.getTypeMirror().getSymbol().getCanonicalName()));
    }

    private boolean isSpringBean(ASTFieldDeclaration node) {
        ASTClassOrInterfaceDeclaration clazz = getAstClassOrInterfaceDeclaration(node);
        var annotations = clazz.getDeclaredAnnotations();
        return annotations.any(annotation -> SPRING_BEAN_ANNOTATIONS.contains(annotation.getAnnotationName()));
    }

    private static ASTClassOrInterfaceDeclaration getAstClassOrInterfaceDeclaration(ASTFieldDeclaration node) {
        var parent = (ASTClassOrInterfaceBody) node.getParent();
        return ((ASTClassOrInterfaceDeclaration) parent.getParent());
    }
}
