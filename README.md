# PMD custom rule-set

## PMD related info

This is extension for PMD tool.
This extension was created for addressing problem with Spring stateful services/beans (https://exas.atlassian.net/browse/GPP-3293).

Since our projects are using JDK 21, it requires to use PMD 7.x (at this time there is only RC versions).

**!NOTE!** PMD 7.x is supporting only CLI runner. Maven PMD plugin is using 6.x (but 6.x versions don't support JDK 21).

**!NOTE!** PMD 7.x has different AST, and it's incompatible with PMD 6.x. 

## Custom validators

- ImmutableSpringBeanRule - validate if Spring service/bean is stateless. It checks is class is annotated with @Component or @Service and has only final fields or fields with @Value or @GrpcClient, otherwise it will fail.
  Supports: `ignoreTests` parameter - if value `true` then classes in testSourceRoot will be skipped, in case of `false` test classes will be scanned as well. 

## How to run

**_For now, it supports only CLI run. Later after PMD 7.x release it should be available through maven PMD plugin as well._**

1. Download and install PMD 7.x CLI: https://pmd.github.io/pmd/pmd_userdocs_installation.html
2. Download and build pmd-ex library (this repo): `mvn clean package`
3. Copy `target/pmd-ex-x.x-SNAPSHOT.jar` into `${PMD_PATH}/lib`. **NOTE:** adding to classpath should be supported through `--aux-classpath` parameter, but it doesn't work in case of PMD v7.0.0.RC4
4. Run command: `pmd -R src/main/resources/pmd/custom-rules.xml -d ${PROJECT_PATH}/application/src -r report.txt`.