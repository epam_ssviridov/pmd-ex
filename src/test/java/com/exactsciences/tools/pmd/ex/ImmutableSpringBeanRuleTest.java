package com.exactsciences.tools.pmd.ex;

import net.sourceforge.pmd.testframework.SimpleAggregatorTst;

public class ImmutableSpringBeanRuleTest extends SimpleAggregatorTst {
    @Override
    public void setUp() {
        addRule("pmd/custom-rules.xml", "SpringBeansShouldBeStateless");
    }
}
